output "vmname" {
	value = "${google_compute_instance.plexvm.name}"
}
output "ExternalIP" {
	value = "${google_compute_address.static_ext_ip.address}"
}
output "CONNECT" {
	value = "ssh ${google_compute_address.static_ext_ip.address} -L 8888:localhost:32400"
}
