terraform {
  backend "gcs" {
    bucket  = "giondo-states"
    prefix  = "terraform/state/plex-server-gcp"
  }
}
