resource "google_compute_instance" "plexvm" {
    name         = "plex-server"
    machine_type = var.plex_instance_type
    tags = ["plex"]
    #allow_stopping_for_update = true

    boot_disk {
      initialize_params {
        image = var.IMAGE_NAME
        size  = 50
      }
    }

    network_interface {
      network = google_compute_network.plex-network.name
      access_config {
            nat_ip = google_compute_address.static_ext_ip.address
        }
      }



    metadata_startup_script = file("scripts/init_script-plex.sh")

    service_account {
        email  = "giondo-media@giondo-backup.iam.gserviceaccount.com"
        scopes = ["userinfo-email", "compute-ro", "storage-ro"]
      }
}
