resource "google_compute_firewall" "default" {
  name    = "firewall-plex"
  network = google_compute_network.plex-network.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "32400"]
  }

  target_tags = ["plex"]
  source_ranges = ["0.0.0.0/0"]
}
