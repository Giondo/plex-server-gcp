#!/bin/bash
#GCSFUSE to mount the GCS Bucket where the movies are
#Add GCSFuse REPO
#https://cloud.google.com/storage/docs/gcs-fuse
export GCSFUSE_REPO=gcsfuse-`lsb_release -c -s`
echo "deb http://packages.cloud.google.com/apt $GCSFUSE_REPO main" | sudo tee /etc/apt/sources.list.d/gcsfuse.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
#Install GCSFUSE
sudo apt-get update -y
sudo apt-get install gcsfuse -y
#Create Folder
sudo mkdir /MEDIA
#Mount /MEDIA folder
gcsfuse --implicit-dirs -o allow_other -file-mode=777 -dir-mode=777 giondo-media-eu /MEDIA
#Add Plex REPO
echo deb https://downloads.plex.tv/repo/deb public main | sudo tee /etc/apt/sources.list.d/plexmediaserver.list
curl https://downloads.plex.tv/plex-keys/PlexSign.key | sudo apt-key add -
#Install Plex Server
sudo apt-get update -y
#deb package also want to replace the .list file on the sources 
sudo apt-get -y -o Dpkg::Options::=--force-confdef -o Dpkg::Options::=--force-confnew install plexmediaserver
