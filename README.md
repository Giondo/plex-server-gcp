# Mount a plex server in GCP
> Create a plex server in GCP as instance and mount a bucket with all media
> allow remote access for seeing content from your house
> after server is created please login and configure it as necessary

## How to gcloud auth

```
gcloud auth application-default login
```
## TO-DO
* Fix fw rules to allow only your external IP

## PREREQUISITES
* bucket created
* service account with access to the bucket to use in the instance

# Tested
- Terraform v0.12.29
